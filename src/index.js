import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import SearchPage from './feature/search/Search'
import registerServiceWorker from './registerServiceWorker'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, compose } from 'redux'
import promiseMiddleware from 'redux-promise-middleware'
import reducer from './feature/search/redux'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const store = createStore(
  reducer,
  composeEnhancers(applyMiddleware(promiseMiddleware()))
)

ReactDOM.render(
  <Provider store={store}>
    <SearchPage />
  </Provider>,
  document.getElementById('root')
)
registerServiceWorker()
