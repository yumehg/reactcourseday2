import React from 'react'
import { Card, Col, Row } from 'antd'
import 'antd/dist/antd.css'

const { Meta } = Card

export const Photos = props => {
  const { myPhoto, myName } = props
  return (
    <div style={{ background: '#ECECEC', padding: '30px' }}>
      <Row gutter={16}>
        <Col span={8}>
          <Card
            hoverable
            style={{ width: 300 }}
            cover={<img alt="example" src={myPhoto} />}
          >
            <Meta title={myName} description="www.instagram.com" />
          </Card>
        </Col>
      </Row>
    </div>
  )
}
export default Photos
