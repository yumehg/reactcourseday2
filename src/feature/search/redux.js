import axios from 'axios'
import config from '../../common/config'

const initialState = {
  searchText: '',
  dataPhoto: []
}

const SEARCH_FIELD = 'SEARCH_FIELD'
const SEARCH = 'SEARCH'
const SEARCH_FULFILLED = 'SEARCH_FULFILLED'

export default (state = initialState, action) => {
  switch (action.type) {
    case SEARCH_FIELD:
      return {
        ...state,
        searchText: action.searchText
      }
    case SEARCH_FULFILLED:
      return {
        ...state,
        dataPhoto: action.payload.data.results
      }
    default:
      return state
  }
}

export const searchField = searchText => ({
  type: SEARCH_FIELD,
  searchText
})

export const search = searchText => ({
  type: SEARCH,
  payload: axios.get(
    `https://api.unsplash.com/search/photos?client_id=${
      config.accessKey
    }&query=${searchText}`
  )
})
