import React from 'react'
import { searchField, search } from './redux'
import { connect } from 'react-redux'
import './search.css'
import 'antd/lib/input/style/css'
import { Input } from 'antd'
import { Row, Col } from 'antd'
import { Photos } from './Photo'

const Search = Input.Search
const enhance = connect(state => state, { searchField, search })

export const SearchPage = props => {
  const { searchText, search, searchField, dataPhoto } = props
  return (
    <section className="section">
      <div className="container">
        <Row>
          <Col span={12} offset={6}>
            <Search
              value={searchText}
              onChange={e => searchField(e.target.value)}
              onSearch={search}
              placeholder="input search text"
              enterButton="Search"
              size="large"
            />
          </Col>
        </Row>
      </div>
      <div className="container-body">
        {dataPhoto &&
          dataPhoto.map(data => {
            return <Photos myPhoto={data.urls.small} myName={data.user.name} />
          })}
      </div>
    </section>
  )
}

export default enhance(SearchPage)
